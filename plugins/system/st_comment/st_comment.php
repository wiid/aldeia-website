<?php
/**
 * @version		1.0.0
 * @copyright 	Beautiful-Templates.com
 * @author		Neo
 * @link		http://www.beautiful-templates.com
 * @license		License GNU General Public License version 2 or later http://www.gnu.org/licenses/gpl-2.0.html
 */

// no direct access
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');
class plgSystemST_Comment extends JPlugin
{
	public function onContentAfterDisplay($context, &$article, &$params, $limitstart=0)
	{
		$app = JFactory::getApplication();
		if ($app->isAdmin()) {
			return;
		}
		
		$shortname = $this->params->get('shortname', '');
		
		if ($shortname == '') {
			return;
		}
		
		if ($context == 'com_content.article') 
		{
			$html = '<div id="disqus_thread"></div>
    				<script type="text/javascript">
        				var disqus_shortname = "'.$shortname.'";
						(function() {
				            var dsq = document.createElement("script"); dsq.type = "text/javascript"; dsq.async = true;
				            dsq.src = "http://beautifultemplates.disqus.com/embed.js";
				            (document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(dsq);
				        })();
				    </script>';
			
			return $html;
		}
	}
}
