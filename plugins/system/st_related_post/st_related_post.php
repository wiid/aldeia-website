<?php
/**
 * @package Related News
 * @version 1.7.0
 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 * @copyright (c) 2012 YouTech Company. All Rights Reserved.
 * @author YouTech Company http://www.smartaddons.com
 *
 */
defined('_JEXEC') or die;
jimport('joomla.plugin.plugin');
$lang = JFactory::getLanguage();
$lang->load('plg_system_st_related_post', JPATH_ADMINISTRATOR);

class plgSystemST_Related_Post extends JPlugin {
	
	/**
	 * Plugin that loads content within content
	 *
	 * @param	string	The context of the content being passed to the plugin.
	 * @param	object	The article object.  Note $article->text is also available
	 * @param	object	The article params
	 * @param	int		The 'page' number
	 */
	 
	public function onContentAfterDisplay($context, &$article, &$params, $limitstart=0) {
		
		$layerOptions=array(
			 'width' => '100%', 
			 'height' => '285px',
			 'cols' => '3',
			 'effects' => 'recentwork', 
			 'layout' => '_slide.php' ,
			 'html' => '1' ,
			 'autoPlay' => '0',
			 'autoPlayDelay' => '5000',
			 'nav' => '1' ,
			 'pagination' =>'0'
		);
		$id = uniqid();

		$document = JFactory::getDocument();
		$document->addCustomTag('<script src="'.JURI::root(true).'/plugins/system/st_related_post/assets/js/jquery.sequence.js" type="text/javascript"></script>');
		$document->addStyleSheet(JUri::base().'plugins/system/st_related_post/assets/css/style.css');
		$document->addScriptDeclaration('
			jQuery.noConflict();
			(function($){
				$(document).ready(function(){
					var layerOptions = '.json_encode($layerOptions).';
					if (layerOptions.nav) {
						layerOptions.nextButton = "#'.$id.' .sequence-next";
						layerOptions.prevButton = "#'.$id.' .sequence-prev";
					}
					
					//layerOptions.preventDelayWhenReversingAnimations = true;
					//layerOptions.transitionThreshold = 3000;
						
					if (layerOptions.pagination) { layerOptions.pagination = "#'.$id.' .sequence-pagination" }
					var options'.$id.' = layerOptions;
			    
			    	var mySequence'.$id.' = $("#'.$id.'").sequence(options'.$id.').data("sequence");
				});
			})(jQuery);
		');
		
		$db = JFactory::getDbo();
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$groups = implode(',', $user->getAuthorisedViewLevels());
		$date = JFactory::getDate();
		$maximum = 5;
		
		$nullDate = $db->getNullDate();
		$now = $date->toSql();
		$related = array();
		$query = $db->getQuery(true);
		if ($context == 'com_content.article') 
		{
		 	
			$temp = JRequest::getVar('id');
			$temp = explode(':', $temp);
			$temp_id = $temp[0];
			
			// select the meta keywords from the item

			$query->select('metakey')
				->from('#__content')
				->where('id = ' . (int) $temp_id);
			$db->setQuery($query);
			
			if ($metakey = trim($db->loadResult()))
			{
				// explode the meta keys on a comma
				$keys = explode(',', $metakey);
				$likes = array();

				// assemble any non-blank word(s)
				foreach ($keys as $key)
				{
					$key = trim($key);
					if ($key)
					{
						$likes[] = $db->escape($key);
					}
				}

				if (count($likes))
				{
					// select other items based on the metakey field 'like' the keys found
					$query->clear()
						->select('a.id')
						->select('a.title')
						->select('DATE_FORMAT(a.created, "%Y-%m-%d") as created')
						->select('a.alias')
						->select('a.catid')
						->select('a.images')
						->select('a.introtext')
						->select('cc.access AS cat_access')
						->select('cc.published AS cat_state');

					// Sqlsrv changes
					$case_when = ' CASE WHEN ';
					$case_when .= $query->charLength('a.alias', '!=', '0');
					$case_when .= ' THEN ';
					$a_id = $query->castAsChar('a.id');
					$case_when .= $query->concatenate(array($a_id, 'a.alias'), ':');
					$case_when .= ' ELSE ';
					$case_when .= $a_id . ' END as slug';
					$query->select($case_when);

					$case_when = ' CASE WHEN ';
					$case_when .= $query->charLength('cc.alias', '!=', '0');
					$case_when .= ' THEN ';
					$c_id = $query->castAsChar('cc.id');
					$case_when .= $query->concatenate(array($c_id, 'cc.alias'), ':');
					$case_when .= ' ELSE ';
					$case_when .= $c_id . ' END as catslug';
					$query->select($case_when)
						->from('#__content AS a')
						->join('LEFT', '#__content_frontpage AS f ON f.content_id = a.id')
						->join('LEFT', '#__categories AS cc ON cc.id = a.catid')
						->where('a.id != ' . (int) $id)
						->where('a.state = 1')
						->where('a.access IN (' . $groups . ')');
					$concat_string = $query->concatenate(array('","', ' REPLACE(a.metakey, ", ", ",")', ' ","'));
					$query->where('(' . $concat_string . ' LIKE "%' . implode('%" OR ' . $concat_string . ' LIKE "%', $likes) . '%")') //remove single space after commas in keywords)
						->where('(a.publish_up = ' . $db->quote($nullDate) . ' OR a.publish_up <= ' . $db->quote($now) . ')')
						->where('(a.publish_down = ' . $db->quote($nullDate) . ' OR a.publish_down >= ' . $db->quote($now) . ')');

					// Filter by language
					if (JLanguageMultilang::isEnabled())
					{
						$query->where('a.language in (' . $db->quote(JFactory::getLanguage()->getTag()) . ',' . $db->quote('*') . ')');
					}

					$db->setQuery($query, 0, $maximum);
					$temp = $db->loadObjectList();

					if (count($temp))
					{
						foreach ($temp as $row)
						{
							if ($row->cat_state == 1)
							{
								$row->route = JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug));
								$related[] = $row;
							}
						}
					}
					unset ($temp);
				}

				$colWidth = 100/3;
				
				$html="";
				$html.="<div class=' st-related-post' >";
				$html.="<h3 class='heading'><span>Related Post</span></h3>";
				$html.="<div class='st-layer-slider layer_layout_slide layer-effect-recentwork' style='height:285px; width:100%' id='".$id."'>";
				$html.="<div class='sequence-prev'></div>";
				$html.="<div class='sequence-next'></div>";
				$html.="<ul class='sequence-canvas'>";
				$colIndex = 0;
				foreach ($related as $key => $item){
					
					if($colIndex < 1){
						$html.="<li>";
					}
						
						$img  = json_decode($item->images,TRUE);
						
						$html.= "<div class='column' style=' width:".$colWidth."%; left: ".$colIndex*$colWidth."%;' >";
						$html.="<div class='column-inner'>";
							$html.="<div class='image'>";
							if (isset($img['image_intro']) && !empty($img['image_intro'])){
									$html.= "<img src='".JUri::base().$img['image_intro']."' alt='".$item->title."'/>";
							}
							$html.="</div>";
							$html.= '<h3 class="title"><a href="'.$item->route.'">'.$item->title.'</a></h3>';
							$html.= "<div class='date'>".JHtml::_('date', $item->created, JText::_('DATE_FORMAT_LC2'))."</div>";
							$html.="<div class='content'>".$item->introtext."</div>";
						$html.="</div>";
						$html.="</div>";
						
					if (($key+1) % 3 < 1 || ($key + 1 == count($related))){
						$html.="</li>";
						$colIndex = 0;
					} else{
						$colIndex ++;
					}
					
				}		
				$html.="</ul>";
				$html.="</div>";		
				$html.="</div>";
					
				return $html;
			}
		
		}
		
		
	}
}