<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_popular
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the syndicate functions only once
require_once __DIR__ . '/helper.php';

$pop_list = ModArticlesPopularHelper::getList($params);
$last_list= ModArticlesLatestHelper::getList($params);
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$document = JFactory::getDocument();
$document->addScriptDeclaration('
	jQuery.noConflict();
	(function($){
		$(document).ready(function(){
		    $(".tab_content:not(:first)").hide();
		    $(".tabs li a").click(function(){
		        $(".tabs li a").removeClass("active");
		        $(this).addClass("active");
		        $(".tab_content").hide();
		        
		        var activetabs = $(this).attr("href");
		        $(activetabs).fadeIn();
		        return false;
		    });
		});
	})(jQuery);
');
?>
<ul class="tabs">
    <li><a href="#popular" class="active">Popular</a></li>
    <li><a href="#lastest">Lastest</a></li>
</ul>

<div class="tab_container">
	<ul class="tab_content popular" id="popular">
	<?php foreach ($pop_list as $pop_item) :?>
		<li>
			<?php echo JLayoutHelper::render('joomla.content.intro_image', $pop_item); ?>
			<a href="<?php echo $pop_item->link; ?>">
				<?php echo $pop_item->title; ?></a>
		</li>
	<?php endforeach; ?>
	</ul>
	
	<ul class="tab_content lastest" id="lastest">
	<?php foreach ($last_list as $last_item) :  ?>
		<li>
			<?php echo JLayoutHelper::render('joomla.content.intro_image', $pop_item); ?>
			<a href="<?php echo $last_item->link; ?>">
				<?php echo $last_item->title; ?></a>
		</li>
	<?php endforeach; ?>
	</ul>
</div>