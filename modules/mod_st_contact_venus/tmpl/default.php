<?php 
$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'modules/mod_st_contact_venus/tmpl/default.css');
?>
<div>
		<form class="st-contact-form" action="<?php echo $url;?>" method="post">
			<div class="name">
				<label>Your Name (required)</label>
				<input  type="text" name="name" value="" /> 
			</div>
			<div class="email">
				<label>Your Email (required)</label>
				<input class="email"  type="text" name="email" value="" /> 
			</div>
			<div class="messages">
				<label>Messages </label>
				<textarea name="messages" rows="15" cols="50"></textarea> 
			</div>
			<input class="submit" type="submit" name="submit" value="Send Messages" />
		</form>
</div>