<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

//test input data
function test_input($data)
{
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

// define url page
$url = JURI::current();

$recipient = $params->get('email_recipient','contact@gmail.com');
$name_from = $params->get('name_from','Admin Beautiful Templates');
$email_from = $params->get('email_from','beautiful-templates@gmail.com');

//define variables and set to empty values
$name = $email = $subject = $content = '';
if (isset($_POST["submit"])){
	$errors = array();
	
	if((isset($_POST["name"]) && $_POST["name"]=='') || (!isset($_POST["name"]))){
		$errors = 'name';
		echo "<p class='error'>".JText::_("ST_ERROR_NAME")."</p>";
	} else{
		$name = test_input($_POST["name"]);
	}
	if((isset($_POST["email"]) && $_POST["email"]=='') || (!isset($_POST["email"]))||(!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/", strtolower($_POST["email"])))){
		$errors = 'email';
		echo "<p class='error'>".JText::_("ST_ERROR_EMAIL")."</p>";
	} else{
		$email = test_input($_POST["email"]);
	}
	/*
	if((isset($_POST["subject"]) && $_POST["subject"]=='') || (!isset($_POST["subject"]))){
		$errors = 'subject';
		echo "<p class='error'>".JText::_("ST_ERROR_SUBJECT")."</p>";
	} else{
		$subject = test_input($_POST["subject"]);
	} 
	 */
	if((isset($_POST["messages"]) && $_POST["messages"]=='') || (!isset($_POST["messages"]))){
		$errors = 'messages';
		echo "<p class='error'>".JText::_("ST_ERROR_CONTENT")."</p>";
	} else{
		$messages = test_input($_POST["messages"]);
	}

	if(empty($errors)){
		$cc=null;
		$bcc=null;
		$attachment =null;
		$mail = & JFactory::getMailer();
		
		$body = "You received a message from ". $email ." with massages :"."\n\n". $messages;
		$subject= 'Hello '.$recipient.'\n';

        $mail->setSubject($subject);
        $mail->setBody($body);
		
		// Are we sending the email as HTML?
       
        $mail->IsHTML(true);
        
		$mail->addRecipient($recipient);
		$mail->setSender(array($email_from, $name_from));
        $mail->addCC($cc);
        $mail->addBCC($bcc);
        $mail->addAttachment($attachment);
		
		$mail->addReplyTo($email,$name);
 
		if(!$mail->Send()) {
			
		  echo "<p class='error'>".JText::_("ST_SENT_FAIL")."</p>". $mail->ErrorInfo;
		} else {
		  echo "<p class='success'>".JText::_("ST_SENT_SUCCESS")."</p>";
		}
		
	}	
}
require JModuleHelper::getLayoutPath('mod_st_contact_venus', $params->get('layout', 'default'));